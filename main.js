// @ts-check
import { initializeApp } from "@firebase/app";
import { createUserWithEmailAndPassword, getAuth, onAuthStateChanged, sendEmailVerification, signInWithEmailAndPassword } from "@firebase/auth";
import { addDoc, collection, doc, getDoc, getDocs, getFirestore, limit, orderBy, query, serverTimestamp, where } from "@firebase/firestore";
import { getFunctions, httpsCallable } from "@firebase/functions";
/**
 * @typedef {import("./types").Post} Post
 * @typedef {import("./types").Profile} Profile
 */
/**
 * @typedef {import("@firebase/firestore").QueryDocumentSnapshot<Post>} PostDocument
 */
import { idToName } from "./idtoname.js";

const app = initializeApp({
  projectId: "bubble-uni-app",
  apiKey: "AIzaSyCnyvPNeBdSDJ-aLY6TnWOusPJS7wTAPhE",
  authDomain: "bubble-uni-app.firebaseapp.com",
  storageBucket: "bubble-uni-app.appspot.com",
  messagingSenderId: "45840221975",
  appId: "1:45840221975:web:95b4762c6f7fb7991cd952",
});
const auth = getAuth(app);
const db = getFirestore(app);
const functions = getFunctions(app, "asia-northeast1");

const signinBox = /** @type {HTMLDivElement} */ (document.getElementById("signin"));
const signinBtn = /** @type {HTMLButtonElement} */ (document.getElementById("signin-btn"));
const emailInput = /** @type {HTMLInputElement} */ (document.getElementById("email"));
const passwordInput = /** @type {HTMLInputElement} */ (document.getElementById("password"));
const signupBtn = /** @type {HTMLButtonElement} */ (document.getElementById("signup-btn"));
const signinError = /** @type {HTMLSpanElement} */ (document.getElementById("signin-error"));

const postBox = /** @type {HTMLDivElement} */ (document.getElementById("post"));
const postInput = /** @type {HTMLInputElement} */ (document.getElementById("post-input"));
const postBtn = /** @type {HTMLButtonElement} */ (document.getElementById("post-btn"));

const timelineBox = /** @type {HTMLDivElement} */ (document.getElementById("timeline"));
const timelinePosts = /** @type {HTMLDivElement} */ (document.getElementById("timeline-posts"));
const signoutBtn = /** @type {HTMLButtonElement} */ (document.getElementById("signout-btn"));
const refreshBtn = /** @type {HTMLButtonElement} */ (document.getElementById("refresh-btn"));
const timelineMoreBtn = /** @type {HTMLButtonElement} */ (document.getElementById("timeline-more-btn"));

const filteredBox = /** @type {HTMLDivElement} */ (document.getElementById("filtered"));
const filteredName = /** @type {HTMLDivElement} */ (document.getElementById("filtered-name"));
const filteredScore = /** @type {HTMLDivElement} */ (document.getElementById("filtered-score"));
const filteredBy = /** @type {HTMLDivElement} */ (document.getElementById("filtered-by"));
const filteredUp = /** @type {HTMLDivElement} */ (document.getElementById("filtered-up"));
const filteredDown = /** @type {HTMLDivElement} */ (document.getElementById("filtered-down"));
const filteredBackBtn = /** @type {HTMLButtonElement} */ (document.getElementById("filtered-back-btn"));

const rankingBtn = /** @type {HTMLButtonElement} */ (document.getElementById("ranking-btn"));
const rankingBox = /** @type {HTMLDivElement} */ (document.getElementById("ranking"));
const rankingProfiles = /** @type {HTMLDivElement} */ (document.getElementById("ranking-profiles"));
const rankingBackBtn = /** @type {HTMLButtonElement} */ (document.getElementById("ranking-back-btn"));
const rankingMoreBtn = /** @type {HTMLButtonElement} */ (document.getElementById("ranking-more-btn"));

signinBtn.addEventListener("click", async (e) => {
  try {
    await signInWithEmailAndPassword(auth, emailInput.value, passwordInput.value);
  } catch (e) {
    signinError.textContent = e.message;
  }
});

signupBtn.addEventListener("click", async (e) => {
  try {
    if(! /^[\w-\.]+@([\w-]+\.)+[\w-]{2,}$/.test(emailInput.value)){
      throw new Error("ERROR: invalid email address!!");
    }
    if(! /^[\w-\.]+@g.ecc.u-tokyo.ac.jp$/.test(emailInput.value)){
      throw new Error("ERROR: invalid email address!");
    }
    await createUserWithEmailAndPassword(auth, emailInput.value, passwordInput.value);
  } catch (e) {
    signinError.textContent = e.message;
  }
});

onAuthStateChanged(auth, async (user) => {
  if (!user) {
    signinBox.style.display = "block";
    timelineBox.style.display = "none";
    postBox.style.display = "none";
  } else if (user.emailVerified) {
    signinBox.style.display = "none";
    timelineBox.style.display = "block";
    postBox.style.display = "block";
    await execHash();
  } else {
    await sendEmailVerification(user);
    signinError.textContent = `Please verify your email address: ${user.email} and sign in.`;
    await auth.signOut();
  }
});

signoutBtn.addEventListener("click", () => {
  auth.signOut();
});

const posts = /** @type {import("@firebase/firestore").CollectionReference<Post>} */ (collection(db, "posts"));

const profiles = /** @type {import("@firebase/firestore").CollectionReference<Profile>} */ (collection(db, "profiles"));

const replies = /** @type {import("@firebase/firestore").CollectionReference<Post>} */ (collection(db, "replies"));

postBtn.addEventListener("click", async () => {
  if (!auth.currentUser) return;
  if (!postInput.value) return;
  await addDoc(posts, {
    uid: auth.currentUser.uid,
    body: postInput.value,
    univ: "UTokyo",
    upvoteCount: 0,
    upvoteUids: [],
    downvoteCount: 0,
    downvoteUids: [],
    bookmarkedUsers: [],
    replyCount: 0,
    expireAt: new Date(Date.now() + 1000 * 60 * 60 * 24 * 30),
    createdDate: new Date(),
    createdAt: serverTimestamp(),
  });
  postInput.value = "";
  await refresh();
});

/**
 * @param {PostDocument} doc
 * @param {string} prefix
 */
function createPostElement(doc, prefix="home") {
  const post = doc.data();
  const postelem = document.createElement("div");
  postelem.id = prefix + "_" + doc.id;
  postelem.className = "post";

  const content = document.createElement("div");
  content.className = "post-content";

  const left = document.createElement("div");
  left.className = "post-left post-inner";
  left.appendChild(createUidButton(post.uid, "uid by"));
  const body = document.createElement("div");
  body.className = "body";
  body.textContent = post.body;
  left.appendChild(body);
  if (post.imageURL !== undefined) {
    const image = document.createElement("img");
    image.className = "thumb";
    image.src = post.imageURL;
    left.appendChild(image);
  }
  const createdAt = document.createElement("div");
  createdAt.className = "created-at";
  createdAt.textContent = post.createdAt.toDate().toLocaleString();
  left.appendChild(createdAt);
  content.appendChild(left);

  const upvoted = auth.currentUser && post.upvoteUids.includes(auth.currentUser.uid);
  const downvoted = auth.currentUser && post.downvoteUids.includes(auth.currentUser.uid);
  const middle = document.createElement("div");
  middle.className = "post-middle post-inner";
  const uvbtn = document.createElement("div");
  uvbtn.textContent = "▲";
  if (upvoted) {
    uvbtn.className = "vote voted vote-upper";
    uvbtn.addEventListener("click", () => cancelvote(doc.id));
  } else {
    uvbtn.className = "vote vote-upper";
    uvbtn.addEventListener("click", () => upvote(doc.id));
  }
  middle.appendChild(uvbtn);
  const score = document.createElement("div");
  score.textContent = "" + (post.upvoteCount - post.downvoteCount);
  if (upvoted || downvoted) {
    score.className = "vote voted";
  } else {
    score.className = "vote";
  }
  middle.appendChild(score);
  const dvbtn = document.createElement("div");
  dvbtn.textContent = "▼";
  if (downvoted) {
    dvbtn.className = "vote voted vote-downer";
    dvbtn.addEventListener("click", () => cancelvote(doc.id));
  } else {
    dvbtn.className = "vote vote-downer";
    dvbtn.addEventListener("click", () => downvote(doc.id));
  }
  middle.appendChild(dvbtn);
  content.appendChild(middle);

  const right = document.createElement("div");
  right.className = "post-right post-inner";
  right.appendChild(document.createTextNode(`upvotes(${post.upvoteCount}):\n`));
  for (const uid of post.upvoteUids) {
    right.appendChild(createUidButton(uid, "uid uv"));
  }
  right.appendChild(document.createTextNode(`downvotes(${post.downvoteCount}):\n`));
  for (const uid of post.downvoteUids) {
    right.appendChild(createUidButton(uid, "uid dv"));
  }
  postelem.appendChild(content);
  postelem.appendChild(right);

  const replies = document.createElement("div");
  replies.className = "replies";
  if (post.replyCount > 0 && post.parentPostId === undefined) {
    const showReplies = document.createElement("a");
    showReplies.className = "replycount";
    showReplies.textContent = `▼返信(${post.replyCount})`;
    showReplies.addEventListener("click", () => {
      popReplyElements(doc.id, replies, prefix);
    });
    replies.appendChild(showReplies);
  }
  postelem.appendChild(replies);

  return postelem;
}

/**
 * @param {string} uid
 * @param {"uid by" | "uid uv" | "uid dv" | "uid rank"} className
 */
function createUidButton(uid, className) {
  const button = document.createElement("button");
  button.className = className;
  button.textContent = `${uid.slice(0, 8)}(${idToName(uid)})`;
  button.addEventListener("click", () => {
    location.hash = `#filter_${encodeURIComponent(uid)}`;
    //filtered(uid);
  });
  return button;
}

async function execHash() {
  if (location.hash.startsWith("#filter_")) {
    filtered(location.hash.slice("#filter_".length));
  } else if (location.hash.startsWith("#ranking")) {
    ranking();
  } else {
    filteredBox.style.display = "none";
    timelineBox.style.display = "block";
    refresh();
  }
}

window.addEventListener("hashchange", () => {
  execHash();
});

const upvoteFunction = httpsCallable(functions, "doUpvotePost");
const downvoteFunction = httpsCallable(functions, "doDownvotePost");
const cancelvoteFunction = httpsCallable(functions, "cancelVotePost");
async function upvote(postid) {
  await upvoteFunction({ postId: postid });
  return refresh();
}
async function downvote(postid) {
  await downvoteFunction({ postId: postid });
  return refresh();
}
async function cancelvote(postid) {
  await cancelvoteFunction({ postId: postid });
  return refresh();
}

let postsCount = 20;

async function refresh() {
  refreshBtn.disabled = true;

  const q = query(posts, where("univ", "==", "UTokyo"), orderBy("createdAt", "desc"), limit(postsCount));
  const querySnapshot = await getDocs(q);
  timelinePosts.innerHTML = "";
  querySnapshot.forEach((doc) => timelinePosts.appendChild(createPostElement(doc, "home")));

  refreshBtn.disabled = false;
}

refreshBtn.addEventListener("click", refresh);
timelineMoreBtn.addEventListener("click", () => {
  postsCount += 20;
  refresh();
});

/**
 * @param {string} uid
 */
async function filtered(uid) {
  timelineBox.style.display = "none";
  filteredBox.style.display = "block";
  rankingBox.style.display = "none";

  const snapshot = await getDoc(doc(profiles, uid));
  const profile = snapshot.data();
  if (profile) {
    filteredName.textContent = `displayName: ${profile.displayName}`;
    filteredScore.textContent = `score: ${profile.score}`;
  }

  const qB = query(posts, where("univ", "==", "UTokyo"), where("uid", "==", uid), orderBy("createdAt", "desc"), limit(20));
  const querySnapshotB = await getDocs(qB);
  showFiltered(querySnapshotB.docs, filteredBy, "posts by", uid);

  const qUD = query(posts, where("univ", "==", "UTokyo"), orderBy("createdAt", "desc"), limit(postsCount));
  const querySnapshotUD = await getDocs(qUD);
  showFiltered(
    querySnapshotUD.docs.filter((doc) => doc.data().upvoteUids.includes(uid)),
    filteredUp,
    "posts upvoted by",
    uid
  );
  showFiltered(
    querySnapshotUD.docs.filter((doc) => doc.data().downvoteUids.includes(uid)),
    filteredDown,
    "posts downvoted by",
    uid
  );
}

/**
 * @param {PostDocument[]} docs
 * @param {HTMLDivElement} element
 * @param {string} h3
 * @param {string} uid
 */
function showFiltered(docs, element, h3, uid) {
  const h3Element = document.createElement("h3");
  h3Element.textContent = `${h3} ${uid.slice(0, 8)}(${idToName(uid)})`;
  element.innerHTML = "";
  element.appendChild(h3Element);
  docs.forEach((doc) => {
    element.appendChild(createPostElement(doc, "filter"));
  });
}

filteredBackBtn.addEventListener("click", () => {
  location.hash = "#";

  filteredBox.style.display = "none";
  timelineBox.style.display = "block";
});

let profilesCount = 20;

async function ranking() {
  location.hash = "#ranking";

  rankingBox.style.display = "block";
  timelineBox.style.display = "none";
  filteredBox.style.display = "none";

  const q = query(profiles, where("univ", "==", "UTokyo"), orderBy("score", "desc"), limit(profilesCount));
  const querySnapshot = await getDocs(q);
  rankingProfiles.innerHTML = "";
  querySnapshot.forEach((doc) => {
    const profile = doc.data();
    const element = document.createElement("div");
    element.className = "profile";
    element.appendChild(createUidButton(doc.id, "uid rank"));
    const text = document.createElement("div");
    text.textContent = `\
displayName: ${profile.displayName}
score: ${profile.score}`;
    element.appendChild(text);
    rankingProfiles.appendChild(element);
  });
}

rankingBtn.addEventListener("click", ranking);

rankingMoreBtn.addEventListener("click", () => {
  profilesCount += 20;
  ranking();
});

rankingBackBtn.addEventListener("click", () => {
  location.hash = "#";

  rankingBox.style.display = "none";
  timelineBox.style.display = "block";
});

/**
 * @param {string} parentPostid
 * @param {HTMLDivElement} parentRepliesElem
 * @param {string} prefix
 */
async function popReplyElements(parentPostid, parentRepliesElem, prefix) {
  const qR = query(replies, where("univ", "==", "UTokyo"), where("parentPostId", "==", parentPostid), limit(20));
  const querySnapshot = await getDocs(qR);
  parentRepliesElem.innerHTML = "";
  const data = [];
  querySnapshot.forEach((doc) => data.push({ doc, post: doc.data() }));

  data.sort((x, y) => x.post.parentReplyIds.length - y.post.parentReplyIds.length);

  data.forEach((d) => {
    let parent = d.post.parentReplyId === undefined ? parentPostid : d.post.parentReplyId;
    document.getElementById(prefix + "_" + parent)?.getElementsByClassName("replies")[0].appendChild(createPostElement(d.doc, prefix));
  });
  //Array.from(parentRepliesElem.getElementsByClassName("replycount")).forEach(e => e.dispatchEvent(new Event('click')));
}
