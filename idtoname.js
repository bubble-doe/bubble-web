function numToName_util(data, num) {
  let tmp = num;
  let name = [];
  data.forEach((d) => {
    name.push(d[tmp % d.length]);
    tmp = Math.floor(tmp / d.length);
  });

  return name.join("");
}

function numToName(num) {
  return numToName_util([data_hira, data_hira, data_hira], num);
}

function idToNum(str) {
  const idChar =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  let num = 0;
  str.split("").forEach((c) => {
    num = num * idChar.length + idChar.indexOf(c);
  });
  return num;
}

export function idToName(id) {
  return numToName(idToNum(id));
}

const data_hira =
  "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん".split(
    ""
  );
