import type { Timestamp } from "@firebase/firestore";

export interface Post {
  uid: string;
  body: string;
  univ: string;
  imageURL?: string;
  createdAt: Timestamp;
  upvoteCount: number;
  upvoteUids: string[];
  downvoteCount: number;
  downvoteUids: string[];
  bookmarkedUsers: string[];
  expireAt: Date;
  createdDate: Date;
  replyCount: number;
  parentPostId?: string;
}

export interface Profile {
  displayName: string;
  score: number;
}